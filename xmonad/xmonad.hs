-- IMPORTS

-- base
import XMonad
import XMonad.Config.Desktop
import qualified XMonad.StackSet as W
import XMonad.Hooks.ManageDocks (avoidStruts, manageDocks, docksEventHook, docks)
import XMonad.Util.NamedScratchpad
-- actions
import XMonad.Actions.WithAll (sinkAll, killAll)
import XMonad.Actions.CycleWS (moveTo, shiftTo, WSType(..), nextScreen, prevScreen)

-- utils
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeysP)

-- data
import qualified Data.Map as M
import Data.Monoid
import Data.Ratio ((%)) -- for video

-- hooks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageHelpers (isFullscreen, isDialog,  doFullFloat, doCenterFloat, doRectFloat)
import XMonad.Util.SpawnOnce
-- layout
import XMonad.Layout.Renamed (renamed, Rename(Replace))
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing 
import XMonad.Layout.GridVariants
import XMonad.Layout.ResizableTile
import XMonad.Layout.BinarySpacePartition
import XMonad.Hooks.InsertPosition (insertPosition, Focus(Newer), Position(End))

-- system
import System.Exit (exitSuccess)
import System.IO (hPutStrLn)

-- variables
myTerminal = "kitty"                   -- default terminal
myScreenshot = "flameshot gui"          -- default screenshot app
myBrowser = "/usr/bin/distrobox-enter -n Arch -- /usr/bin/vivaldi-stable"               -- default browser
myTodo = "flatpak run com.ticktick.TickTick"               -- default browser
myDiscord = "discord"               -- default browser

myBorderWidth = 3                   -- width of window border
myModMask = mod4Mask                -- default mod key

myNormColor = "#cfc9c2"             -- unfocus window border
myFocusColor = "#e0af68"            -- focus window border
myppCurrent = "#ff9e64"             -- current workspace in xmobar
myppVisible = "#cfc9c2"             -- visible but not current workspace
myppHidden = "#a3be8c"              -- hidden workspace in xmobar
myppHiddenNoWindows = "#d8dee9"     -- hidden workspace(no windows)
myppTitle = "#bb9af7"               -- active window title in xmobar
myppUrgent = "#f7768e"              -- urgen workspace

-- workspace
xmobarEscape = concatMap doubleLts
  where doubleLts '<' = "<<"
        doubleLts x   = [x]

--myWorkspaces = clickable . (map xmobarEscape) $ [" 1 "," 2 "," 3 "," 4 "," 5 "," 6 "," 7 "," 8 "," 9 "]
myWorkspaces = clickable . (map xmobarEscape) $ [" \984479 "," \62601 "," \62602 "," \987057 "," \983502 "," \984423 "," \59007 "," \58878 "," \984687 "]
--myWorkspaces = clickable . (map xmobarEscape) $ [" \61820 "," \62057 "," \61728 "," \61485 "," \61729 "," \61734 "," \62081 "," \61799 "," \61459 "]
  where
        clickable l = [ "<action=xdotool key super+" ++ show (n) ++ ">" ++ ws ++ "</action>" |
                      (i,ws) <- zip [1..9] l,
                      let n = i ]

--windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

-- window rules
myManageHook = composeAll
    [ className =? "gpick"   --> doFloat
    , className =? "Zenity" --> doFloat
    , title =? "pulsemixer" --> doRectFloat (W.RationalRect (1 % 4) (1 % 4) (1 % 2) (1 % 2))
    , isFullscreen --> doFullFloat
    ]

--startuphook
myStartupHook :: X ()
myStartupHook = do
   spawnOnce "~/.config/xmonad/scripts/autostart.sh"


-- scratchPads
scratchpads :: [NamedScratchpad]
scratchpads = [
-- run htop in xterm, find it by title, use default floating window placement
    NS "term2" "kitty -T term2" (title =? "term2")
        (customFloating $ W.RationalRect (1/12) (1/6) (5/6) (4/6)),

    NS "term1" "kitty -T term1" (title =? "term1")
        (customFloating $ W.RationalRect (1/12) (1/6) (5/6) (4/6)),
    NS "pulse" "kitty -T pulse -e pulsemixer" (title =? "pulse")
        (customFloating $ W.RationalRect (1/12) (1/6) (5/6) (4/6)),
    NS "music" "kitty -T music -e ncspot" (title =? "music")
        (customFloating $ W.RationalRect (1/12) (1/6) (5/6) (4/6)),
    NS "news" "kitty -T news -e newsboat" (title =? "news")
        (customFloating $ W.RationalRect (1/12) (1/6) (5/6) (4/6)),
    NS "ranger" "kitty --class ranger -e ranger" (className =? "ranger")
        (customFloating $ W.RationalRect (1/12) (1/6) (5/6) (4/6))
  ]

-- keybinds
myKeys =
    [ ("M-S-r", spawn "xmonad --recompile; xmonad --restart")    
    , ("M-S-<Escape>", io exitSuccess)                           
    , ("M-<Return>", spawn myTerminal)                           
    , ("M-d", spawn "rofi -show drun")                           
    , ("M-q", kill)                                              
    , ("M-S-q", killAll)                                         
    , ("M-<Tab>", nextScreen)                                     
    , ("M-S-<Tab>", prevScreen)                                   
    , ("M-h", sendMessage Shrink)                                 
    , ("M-l", sendMessage Expand)                                 
    , ("M-S-.", sendMessage (IncMasterN (-1)))                    
    , ("M-S-,", sendMessage (IncMasterN 1))                       
    , ("M-p", windows W.focusMaster)                              
    , ("M-j", windows W.focusDown)                                
    , ("M-k", windows W.focusUp)                                  
    , ("M-<Space>", sendMessage NextLayout)                       
    , ("M-t", withFocused $ windows . W.sink)                     
    , ("M-S-t", sinkAll)                                          
    , ("M-<F8>", spawn (myTerminal ++ " -e pulsemixer"))          
    , ("M-", spawn (myTerminal ++ " -e pulsemixer"))              
    , ("M-=", spawn ("pamixer --allow-boost -i 3"))               
    , ("M--", spawn ("pamixer --allow-boost -d 3"))               
    , ("M-S-p", spawn (myTerminal ++ " -e ncmpcpp"))              
    , ("M1-C-s", spawn (myScreenshot))                         -- ncmpcpp
    , ("M-w", spawn "/usr/bin/distrobox-enter -n Arch -- /usr/bin/vivaldi-stable") 
    , ("M-<Escape>", spawn "dprompt \"kill Xorg?\" \"killall Xorg\"")       
    , ("M-<F12>", spawn (myTerminal ++ " -e tremc"))                        
    , ("M-n", namedScratchpadAction scratchpads "term1")
    , ("M-S-n", namedScratchpadAction scratchpads "term2")                  
    , ("M-v", namedScratchpadAction scratchpads "pulse")                    
    , ("M-c", namedScratchpadAction scratchpads "ranger")                   
    , ("M-m", namedScratchpadAction scratchpads "music")                    
    , ("M-b", namedScratchpadAction scratchpads "news")                     
-- Apps
    , ("M1-s", spawn (myTodo))
    , ("M1-n", spawn (myDiscord))
    ]



-- layout
myLayout = avoidStruts (tiled ||| full ||| grid ||| bsp)
    where
        -- full
        full = renamed [Replace "F"]
                $ noBorders (Full)
        -- tiled
        tiled = renamed [Replace "||"]
                $ spacingRaw False (Border 10 0 10 0) True (Border 0 10 0 10) True
                $ ResizableTall 1 (3/100) (1/2) []
        -- grid
        grid = renamed [Replace "#"] 
                $ spacingRaw True (Border 10 0 10 0) True (Border 0 10 0 10) True 
                $ Grid (16/10)
        -- bsp
        bsp = renamed [Replace "BSP"]
                $ emptyBSP
        -- default number of windows in master pane
        nmaster = 1
        -- default proportion of screen occupied by master pane
        ratio = 1/2
        -- percent of screen to increment by when resizing panes
        delta = 3/100

-- main
main = do
    xmproc0 <- spawnPipe "xmobar -x 0 $HOME/.xmobarrc"
    xmproc1 <- spawnPipe "xmobar -x 1 $HOME/.xmobarrc"
    xmonad $ ewmh $ docks $ def
        { manageHook = ( isFullscreen --> doFullFloat ) <+> myManageHook <+>  namedScratchpadManageHook scratchpads <+> manageHook desktopConfig <+> manageDocks <+> insertPosition End Newer
        , terminal = myTerminal
        , workspaces = myWorkspaces
        , borderWidth = myBorderWidth
        , startupHook = myStartupHook
        , normalBorderColor = myNormColor
        , focusedBorderColor = myFocusColor
        , layoutHook = smartBorders $ myLayout
        , logHook = dynamicLogWithPP . filterOutWsPP [scratchpadWorkspaceTag] $ xmobarPP  
                        { ppOutput = \x -> hPutStrLn xmproc0 x  >> hPutStrLn xmproc1 x
                        , ppCurrent = xmobarColor myppCurrent "" . wrap "" ""
                        , ppVisible = xmobarColor myppVisible ""
                        , ppHidden = xmobarColor myppHidden "" . wrap "" ""
--                        , ppHiddenNoWindows = xmobarColor  myppHiddenNoWindows ""
                        , ppTitle = xmobarColor myppTitle "" . shorten 30
                        , ppSep = "-"
                        , ppUrgent = xmobarColor myppUrgent "" . wrap "!" "!"
                        , ppOrder  = \(ws:l:_) -> [ws,l]
                        }
        , modMask = myModMask
        }
        `additionalKeysP` myKeys
